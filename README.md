# Seven Segments Clock

I'm sharing all the files needed to build this seven segments clock.
It is a 3D printed clock with laser cut acrylic segment covers and WS2812 leds.

It controlled by an ESP8266 and has a RTC module, logic level  
converter module, and a LDR for dimming the light.
### You can check my youtube video describing very briefly the build process
[![Video](https://i.ytimg.com/vi/vkq0RFj5_Ug/hqdefault.jpg)](https://www.youtube.com/watch?v=vkq0RFj5_Ug)


The clock code is still in progress but for now it is possible  
to change the color and dimming level from the web.

It is also controlled by alexa (I'm adding the skill too)

```
Alexa, ask the clock to change the color to {Green/Red/Blue/White}.
Alexa, ask the clock to {Start/Stop/Pause/Resume} a stopwatch.
Alexa, ask the clock to Start a {5 mintues/70 seconds/1 hour} countdown.
Alexa, ask the clock to {Stop/Pause/Resume} the countdowm.
```

## Hardware
* Wemos D1 Mini
* 28 x 8 channel WS2812 boards.([Like this one](https://www.aliexpress.com/item/32773366821.html)
* 2 x 4 Bit WS2812 boards. ([Like this one](https://www.aliexpress.com/item/32758176722.html)
* 5528 5mm LDR
* DS3231 RTC Module
* 10k resistor
* TXS0108E Logic Level Converter Module
* 60 x M2.5 4mm screws
* 30 x M3 5mm screws
* 4 x M2 35MM screws
* 20 x M3 4mm knurled nut
* 5V 6A Power Suply

## Prerequisites
* [ESP8266 core 2.7.3+ for the Arduino](https://github.com/esp8266/Arduino)
* [Adafruit NeoPixel Library ](https://github.com/adafruit/Adafruit_NeoPixel)
* [RTClib](https://github.com/adafruit/RTClib)
* [ESP8266TimerInterrupt](https://github.com/khoih-prog/ESP8266TimerInterrupt)
* [Arduino Client for MQTT](https://github.com/knolleary/pubsubclient)
* MQTT Broker (For the alexa skill)
 you can have your own or use free services like [cloudmqtt](https://www.cloudmqtt.com/)


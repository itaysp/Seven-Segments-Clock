//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 18.9.2020				//
// File Name: clock.h					//
//////////////////////////////////////////

#ifndef _CLOCK_h
#define _CLOCK_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "segments.h"
#include "RTClib.h"
#include <Adafruit_NeoPixel.h>
#include "types.h"
#include <EEPROM.h>

//Global Variables
extern RTC_DS3231 rtc;
extern clock_digit clock_digits[];
extern ClockStates clockState;
extern uint8_t dim_level;
extern states stopWatchState;
extern states countDownState;

//Function Declecations
void manage_clock();
void digit_test(uint8_t d);
void flash_dot(uint8_t d);
void manage_countdown_timer();
void manage_stopwatch();
void set_starting_point(uint8 m, uint8_t s);
void split_digits(uint8_t m, uint8_t s);
void update_clock_digits(bool dot = false);
void buildTimeDigits();
rgb_color readEepromColor();
void updateEepromColor(rgb_color c);
bool colorCompare(rgb_color c1, rgb_color c2);
void myDelay(uint64_t ms);
#endif

// types.h

#ifndef _TYPES_h
#define _TYPES_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "Adafruit_NeoPixel.h"

struct rgb_color
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
};



// Struct of a digit
struct clock_digit {
	uint8_t gpio;
	uint8_t segment_pixels;
	uint8_t seg_A_start;		//start position of seg A
	uint8_t seg_B_start;		//start position of seg B
	uint8_t seg_C_start;		//start position of seg C
	uint8_t seg_D_start;		//start position of seg D
	uint8_t seg_E_start;		//start position of seg E
	uint8_t seg_F_start;		//start position of seg F
	uint8_t seg_G_start;		//start position of seg G
	uint8_t seg_DOT_start;		//start position of seg G
	Adafruit_NeoPixel strip;	//neopixel strip
};


//States (for state machine)
typedef enum ClockStates
{
	state_idle = 0,
	state_clock,
	state_stopwatch,
	state_countdown,	
}ClockState;

typedef enum states
{
	start = 0x01,
	stop = 0x02,
	pause = 0x03,
	resume = 0x04
}states;


#endif

//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 19.9.2019        //
// File Name: clockTime.cpp				//	
// File Version: 1.3    				//
//////////////////////////////////////////

#include "clockTime.h"
#include "ESP8266TimerInterrupt.h"

RTC_DS3231 rtc;
ESP8266Timer ITimer;
uint8_t dstEnabled = DST;
volatile uint8_t time_h = 0, time_m = 0, time_s = 0;

#ifdef ENABLE_WIFI
	WiFiUDP udp;
	unsigned int localPort = 2390;      // local port to listen for UDP packets
	const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
	byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets											// A UDP instance to let us send and receive packets over UDP
#endif //ENABLE_WIFI

//Init the RTC module
void setupRTC() {
  char rtcTime[11];
	if (!rtc.begin()) {
		Serial.println("Couldn't find RTC");
	  return;
	}
 
	/* if (rtc.lostPower()) {
	rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
	}*/

  //get time from RTC
  while(getClockTime()){
    myDelay(200);
  }
  snprintf(rtcTime, sizeof(rtcTime), "%02u:%02u:%02u", time_h, time_m, time_s);

  Serial.print("RTC Time: ");
  Serial.println(rtcTime);
  
 //setup ISR for time update
 // Interval in microsecs
 ITimer.attachInterruptInterval(1000 * 1000, clockTick);
}

bool setupNTP() {
#ifdef ENABLE_WIFI
		return udp.begin(localPort); 		//for ntp time
#endif //ENABLE_WIFI
}

#ifdef ENABLE_WIFI

//read if the dst is enabled from the eeprom and change the state accordinly
void initDST() { 
	EEPROM.begin(512);
	//check if the eeprom was initzlied with DST value (this flag is at address 1)
	if (EEPROM.read(1) != 1) {
		//eeprom was not initzlided, store the value from the settings.h
		EEPROM.write(0, DST);
		EEPROM.write(1, 1); //set the dst initzlied flag
	}
	dstEnabled = EEPROM.read(0); //Address 0 stores the DST configuration

	EEPROM.end();
}

//All the NTP functions ard based on NTPClient.ino example from the ESP8266WiFi library
void ntpRequestTime()
{
	const char* ntpServerName = NTP_SERVER;
	IPAddress timeServerIP;				// time.nist.gov NTP server address
	WiFi.hostByName(ntpServerName, timeServerIP);
	sendNTPpacket(timeServerIP);
}

//checks if the udp packet arrived and parse it to hours, minutes, seconds
void ntpParseTime()
{
	uint32_t ntp_m = 0, ntp_h = 0, ntp_s = 0;

	int cb = udp.parsePacket();
	if (cb)
	{
		udp.read(packetBuffer, NTP_PACKET_SIZE);
		unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
		unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
		// combine the four bytes (two words) into a long integer
		// this is NTP time (seconds since Jan 1 1900):
		unsigned long secsSince1900 = highWord << 16 | lowWord;
		const unsigned long seventyYears = 2208988800UL;

		unsigned long epoch = secsSince1900 - seventyYears;

		Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)

		ntp_h = ((epoch % 86400L) / 3600) + NTP_GMT;
		if (dstEnabled == true)
		{
			ntp_h += 1; //Daylight saving time
		}
		
		ntp_m = ((epoch % 3600) / 60);

		//taking into account the gmt addition can make the hour larger than 23;
		if (ntp_h > 23){
			ntp_h = ntp_h % 24;
		}
		Serial.print(ntp_h); // print the hour (86400 equals secs per day)
		Serial.print(':');


		if (ntp_m < 10) {
			// In the first 10 minutes of each hour, we'll want a leading '0'
			Serial.print('0');
		}
		ntp_s = (epoch % 3600) / 60;
		Serial.print(ntp_s); // print the minute (3600 equals secs per minute)
		Serial.print(':');
		if ((epoch % 60) < 10) {
			// In the first 10 seconds of each minute, we'll want a leading '0'
			Serial.print('0');
		}
		Serial.println(epoch % 60); // print the second

		ntpTimeUpdate(ntp_h, ntp_m, ntp_s); // update the rtc module time if needed
	}
}

//gets a new time and checks if it's different then
//the current rtc module time, it if does - it adjusts the time.
void ntpTimeUpdate(uint8_t h, uint8_t m, uint8_t s)
{
	DateTime now = rtc.now();
	if ((now.hour() != h || now.minute() != m) || rtc.lostPower()) //time is different or battery removed
	{
		Serial.println("Updating the time from NTP server!");
		rtc.adjust(DateTime(now.year(), now.month(), now.day(), h, m, s));

		//Update the clock digits display
		buildTimeDigits();
	}
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress& address)
{
	Serial.println("sending NTP packet...");
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
							 // 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12] = 49;
	packetBuffer[13] = 0x4E;
	packetBuffer[14] = 49;
	packetBuffer[15] = 52;

	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	udp.beginPacket(address, 123); //NTP requests are to port 123
	udp.write(packetBuffer, NTP_PACKET_SIZE);
	udp.endPacket();
}

#endif //ENABLE_WIFI


int getClockTime(){
	DateTime now = rtc.now();
  if(now.hour() == 0 && now.minute() == 0 && now.second() == 0){
    // probably a reading error, try again later
      return 1;
  }
 
	time_h = now.hour();
	time_m = now.minute();
	time_s = now.second();

  return 0;
}

void clockTick(){
	time_s++;
	
	if(time_s > 59){
		time_s = 0;
		time_m++;
	}

	if(time_m > 59){
		time_m = 0;
		time_h++;
	}

	if(time_h > 23){
		time_h = 0;
	}
}

// 
// 
// 

#include "effects.h"
//Channel fading effect (increased then decreased the channel brightness)
void blueChannelBreathing(uint16_t ms, uint8_t minVal, uint8_t maxVal)
{
	static uint64_t t = millis();
	static bool rising = true;

	if (millis() - t > ms)
	{
		if (rising == true)
		{
			if (clock_color.b < maxVal)
			{
				clock_color.b += 1;
			}
			else
			{
				rising = false;
			}
		}
		else
		{
			if (clock_color.b > minVal)
			{
				clock_color.b -= 1;
			}
			else
			{
				rising = true;
			}
		}
		t = millis();
	}
}
void greenChannelBreathing(uint16_t ms, uint8_t minVal, uint8_t maxVal)
{
	static uint64_t t = millis();
	static bool rising = true;

	if (millis() - t > ms)
	{
		if (rising == true)
		{
			if (clock_color.g < maxVal)
			{
				clock_color.g += 1;
			}
			else
			{
				rising = false;
			}
		}
		else
		{
			if (clock_color.g > minVal)
			{
				clock_color.g -= 1;
			}
			else
			{
				rising = true;
			}
		}
		t = millis();
	}
}
void redChannelBreathing(uint16_t ms, uint8_t minVal, uint8_t maxVal)
{
	static uint64_t t = millis();
	static bool rising = true;

	if (millis() - t > ms)
	{
		if (rising == true)
		{
			if (clock_color.r < maxVal)
			{
				clock_color.r += 1;
			}
			else
			{
				rising = false;
			}
		}
		else
		{
			if (clock_color.r > minVal)
			{
				clock_color.r -= 1;
			}
			else
			{
				rising = true;
			}
		}
		t = millis();
	}
}
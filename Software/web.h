////////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 8.9.2017				//
// File Name: web.h					//
//////////////////////////////////////////

#ifndef _WEB_h
#define _WEB_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include <ESP8266WebServer.h>
#include "types.h"
#include "clock.h"
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include "settings.h"
#include "html.h"
#include "clockTime.h"


#ifdef ENABLE_MQTT
	#include <PubSubClient.h>
#endif

bool setupWifi();	//setup servers, mqtt etc after wifi is connected
void handleNotFound();
void handle_root();
void mqttCallback(char* topic, byte* payload, unsigned int length);

void setupMQTT();
void handleMQTT();
void mqttReconnect();
void handleWebClient();
void setupHttpUpdaer();
bool wifiConnect();	//connect to WiFi network
bool isWifiConnected(); //return true of false if wifi is conencted
#endif

//////////////////////////////////////////////
//		Autor:			Itay Sperling		
// Last Update: 19.9.2019        
//		Verion:			1.4					
//											
//			Seven Segments Clock			
//											
//////////////////////////////////////////////

#include "effects.h"
#include "settings.h"
#include "clock.h"
#include "segments.h"
#include "clockTime.h"
#include "ESP8266TimerInterrupt.h"
#ifdef ENABLE_WIFI
	#include "web.h"
	#include <ArduinoOTA.h>
#endif //ENABLE_WIFI

#if !defined(ESP8266)
#error This code is designed to run on ESP8266 and ESP8266-based boards! Please check your Tools->Board setting.
#endif

extern ESP8266Timer ITimer;

void setup() {
#ifdef ENABLE_DUBUG
	Serial.begin(115200);
  myDelay(500);
#endif //ENABLE_DUBUG

/* This part is half blocking.. while no wifi - only rtc clock is working. */
	//rtc module
	setupRTC();
  
	//Init the DST settings
	initDST();
  
  //init clock leds 
	setupPixels();

	manage_dimming();
	update_dimming();

#ifdef ENABLE_WIFI
	do {
		manage_clock();
		ESP.wdtFeed();
		delay(200); //fast enough to get dots blinks every 1s
	} while (false == setupWifi());

	setupNTP();

#ifdef ENABLE_OTA
	if (isWifiConnected()) {
		setupOTA();
	}
#endif //ENABLE_OTA

#ifdef ENABLE_MQTT
	if (isWifiConnected()) {
		setupMQTT();
	}
#endif //ENABLE_MQTT

#endif //ENABLE_WIFI

#ifdef ENABLE_WIFI
	//make request to the NTP server
	if(isWifiConnected()){
		ntpRequestTime();
	}
#endif //ENABLE_WIFI

}

void loop() {
#ifdef ENABLE_WIFI
		static uint64_t ntp_update_scheduler = millis();

	if(isWifiConnected()){	//only if wifi is connected
		//ESP.wdtFeed();

		#ifdef ENABLE_OTA
			ArduinoOTA.handle();
		#endif //ENABLE_OTA

		#ifdef ENABLE_WEBSERVER
			handleWebClient();
		#endif //ENABLE_WEBSERVER

		#ifdef ENABLE_MQTT
			handleMQTT();
		#endif //ENABLE_MQTT
	
		//ask for time updates every 6 hours
		if (millis() - ntp_update_scheduler > (NTP_UPDATE_INTERVAL)) //sync time every xxx time
		{
			ntpRequestTime();
			ntp_update_scheduler = millis();
		}

		//Check if time packed arrived and parse it
		ntpParseTime();
	}
	else { //if there is no wifi, and wifi is enabled, try to reconnect
		ESP.wdtFeed();
		wifiReconnect();
		delay(200);
	}
#endif 
//ENABLE_WIFI

	switch (clockState)
	{
	case state_clock:
		manage_clock();
		break;
	case state_stopwatch:
		manage_stopwatch();
		break;
	case state_countdown:
		manage_countdown_timer();
		break;

	default:
		break;
	}

	//Effects
	//Channel fading effect (increased then decreased the channel brighness)
	//redChannelBreathing(15,30,240);
	//greenChannelBreathing(25,50,200);
	//blueChannelBreathing(35,10,210);
}


#ifdef ENABLE_WIFI

// Init the OTA
void setupOTA(){
	ArduinoOTA.setHostname("SegmentsClock");
	ArduinoOTA.begin();
	ArduinoOTA.onStart([]() {
		ITimer.detachInterrupt();
	});
  ArduinoOTA.onError([](ota_error_t error) {
      ITimer.reattachInterrupt();
  });
}

void wifiReconnect()
{
	static uint64_t lastReconnect = millis();
	if (millis() - lastReconnect > 60000U) { //try every 1 minutes
		setupWifi(); //connect and setup webserver again
		if (isWifiConnected()) { //connection succeed
			//setup OTA again
			setupOTA();

		#ifdef ENABLE_MQTT
					//setup MQTT again
					setupMQTT();
		#endif //ENABLE_MQTT
		}

		lastReconnect = millis();
	}
}
#endif

//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 30.8.2017				//
// File Name: effects.h					//
//////////////////////////////////////////
#ifndef _EFFECTS_h
#define _EFFECTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "segments.h"
extern struct rgb_color clock_color; //current clock color

//Function Declecations
//Channel fading effect (increased then decreased the channel brightness)
void blueChannelBreathing(uint16_t ms=15, uint8_t minVal = 10, uint8_t maxVal = 255); //time between channel level increase/decrease
void greenChannelBreathing(uint16_t ms=15, uint8_t minVal = 10, uint8_t maxVal = 255);
void redChannelBreathing(uint16_t ms=15, uint8_t minVal = 10, uint8_t maxVal = 255);

#endif


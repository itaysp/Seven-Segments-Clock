//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 24.3.2018				//
// File Name: html.h					//
//////////////////////////////////////////
const char html0[] = R"=====(<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel = "stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jscolor/2.3.3/jscolor.min.js"></script>
</style>
</head>
<body>

<div style="max-width:400px; margin: auto; text-align: center;">
<h1 style="text-align: center;"> Seven Segments Clock </h1>
<h2 style="color: #006bb3;">)=====";

const char html1[] = R"=====(</h2>
<h3 style="text-align: center;">Color</h3>

<input data-jscolor="{width:250, height:300, onInput:'updateColor(this)'}"  value="rgb()=====";

const char html2[] = R"=====()" id="rgb" readonly="true">
<br>

<h3 style="text-align: center;">Dimming</h3>
<input id="dimming" type="range" min="0" max="100" step="5" onchange="updadeDimLevel(this.value)"/>

<script>
function updateColor(picker) {
window.open("?r=" + Math.round(picker.channels.r) + "&g=" +  
Math.round(picker.channels.g) + "&b=" + Math.round(picker.channels.b), "hidden-form");
}

function updadeDimLevel(level) {
window.open("?dim="+level, "hidden-form");
}
</script>

<h3>DST</h3>
<input type="checkbox" name="dst" id="dstCheckbox" onclick="updadeDST(this.value)" )=====";

const char html3[] = R"=====( /> 
<script>
function updadeDST(val) {
 if (document.getElementById('dstCheckbox').checked)
	{ 
		window.open("?dst=1","hidden-form");
	}
	else
	{
		window.open("?dst=0","hidden-form");
	}
location.reload();
}
</script>
</div>
<IFRAME style="display:none" name="hidden-form"></IFRAME>
</body>
</html>)=====";

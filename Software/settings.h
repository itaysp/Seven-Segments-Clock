//////////////////////////////////////////
// Autor: Itay Sperling					
// Last Update: 19.9.2019        
// File Name: settings.h				
// File Version: 1.4    				
//////////////////////////////////////////

#ifndef _SETTINGS_h
#define _SETTINGS_h

/* Clock Features */
//Comment to disable a feature
#define ENABLE_WIFI
	#ifdef ENABLE_WIFI
		#define ENABLE_OTA
		#define ENABLE_WEBSERVER
		#define ENABLE_MQTT			//for the alexa skill
	#endif

//#define ENABLE_DUBUG

#define VER 1.4
#define CLOCK_NAME "SevenSegClock" //max 15 chars

/* Wifi Settings */
#define WIFI_SSID  "HakunaMatata" //  your network SSID (name)
#define WIFI_PASS  "1989Matata!"  // your network password

/* NTP and Time Settings */
#define NTP_SERVER "time.nist.gov"
#define NTP_GMT	2		// change your GMT here. (write - if your are in GMT -3)
#define NTP_UPDATE_INTERVAL (6 * 60 * 60 * 1000U) // 6 hours
#define DST	false		//init the dst state (can be change from web browser)

/* Clock Settings */
#define DIGIT_1_PIN 14
#define DIGIT_2_PIN 0
#define DIGIT_3_PIN 12
#define DIGIT_4_PIN 13
#define DEFAULT_CLOCK_COLOR {0,255,0} //R,G,B

//LDR sensor - envoirment light levels to dim the light 
#define MIN_LDR_THRESH 600    //max val - 1024 (birghtest light)
#define MAX_LDR_THRESH 960    //max val - 1024 (lowest light)
#define BRIGHT_MIN_VALUE 30   //min val - 30
#define BRIGHT_MED_VALUE 100  //min val - 30
#define BRIGHT_MAX_VALUE 255  //max val - 255

/* Mqtt Settings */
#define MQTT_SERVER "192.168.1.100"
#define MQTT_CLIENTUSERNAME "sevenSegClock"
#define MQTT_CLIENTID "seven_seg_clock"
#define MQTT_CLIENTPASS "SEVENSeg@@"
#define MQTT_PORT 1883
#define DEVICE_LOCATION "living_room"
#define MQTT_TOPIC DEVICE_LOCATION "/sevenSegmentsClock/#"
#define MQTT_TOPIC_RGB DEVICE_LOCATION "/sevenSegmentsClock/RGB"
#define MQTT_TOPIC_COLOR DEVICE_LOCATION "/sevenSegmentsClock/color"
#define MQTT_TOPIC_BRIGHTNESS DEVICE_LOCATION "/sevenSegmentsClock/brightness"
#define MQTT_TOPIC_STOPWATCH DEVICE_LOCATION "/sevenSegmentsClock/stopwatch"
#define MQTT_TOPIC_COUNTDOWN_START DEVICE_LOCATION "/sevenSegmentsClock/countdown/start"
#define MQTT_TOPIC_COUNTDOWN DEVICE_LOCATION "/sevenSegmentsClock/countdown"


#endif

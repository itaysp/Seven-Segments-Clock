//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 19.9.2019        //
// File Name: clockTime.h				//
//////////////////////////////////////////

#ifndef _CLOCKTIME_h
#define _CLOCKTIME_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include "settings.h"
#include "RTClib.h"
#include <EEPROM.h>
#include "clock.h"


#ifdef ENABLE_WIFI
	#include "ESP8266WiFi.h"
	#include <WiFiUdp.h>
#endif //ENABLE_WIFI

//Function Declecations
void setupRTC();
int getClockTime();
void clockTick();
#ifdef ENABLE_WIFI
	void ntpParseTime();
	void sendNTPpacket(IPAddress& address);
	void ntpTimeUpdate(uint8_t h, uint8_t m, uint8_t s);
	void ntpRequestTime();
	void initDST();
	bool setupNTP();
#endif //ENABLE_WIFI
#endif

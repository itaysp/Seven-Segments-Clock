//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 30.8.2017				//
// File Name: segments.cpp				//
//////////////////////////////////////////

#include "segments.h"

/*
 SEVEN SEGMENTS
       A
    ---------
   |         |
 F |         | B
   |    G    |
    ---------
   |         |
 E |         | C
   |         |
    ---------
       D         */

const struct rgb_color color_red = { 255,0,0 };
const struct rgb_color color_green = { 0,255,0 };
const struct rgb_color color_blue = { 0,0,255 };
const struct rgb_color color_white = { 255,255,255 };

struct rgb_color clock_color = DEFAULT_CLOCK_COLOR;

// Digits
clock_digit clock_digits[NUM_OF_DIGITS]{
	{ DIGIT_1_PIN,8,24,16,8,0,40,32,48, 88 ,Adafruit_NeoPixel(7 * 8, DIGIT_1_PIN, NEO_GRB + NEO_KHZ800) },
	{ DIGIT_2_PIN,8,24,16,8,0,40,32,48, 56 ,Adafruit_NeoPixel((7 * 8) + 4, DIGIT_2_PIN, NEO_GRB + NEO_KHZ800) },
	{ DIGIT_3_PIN,8,24,16,8,0,40,32,48, 56 ,Adafruit_NeoPixel((7 * 8) + 4, DIGIT_3_PIN, NEO_GRB + NEO_KHZ800) },
	{ DIGIT_4_PIN,8,24,16,8,0,40,32,48, 99 ,Adafruit_NeoPixel(7 * 8, DIGIT_4_PIN, NEO_GRB + NEO_KHZ800) },
};

//Init the WS2812 leds
void setupPixels() {
	rgb_color temp_color;
	// init neopixel digits
	for (int i = 0; i < NUM_OF_DIGITS; i++) {
		clock_digits[i].strip.begin();
	}

	temp_color = readEepromColor();
	if (!colorCompare(temp_color, clock_color)) {
		clock_color = temp_color;
	}

	manage_dimming();
	update_dimming();
}

//Turn on/off segments to lit a number
void setNumber(struct clock_digit *d, struct rgb_color c, uint8_t n, bool widthDot)
{
	switch (n)
	{
	case 1:
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		break;

	case 2:
		setSegment(d, c, 'a');
		setSegment(d, c, 'b');
		setSegment(d, c, 'g');
		setSegment(d, c, 'e');
		setSegment(d, c, 'd');
		break;

	case 3:
		setSegment(d, c, 'a');
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		setSegment(d, c, 'g');
		setSegment(d, c, 'd');
		break;


	case 4:
		setSegment(d, c, 'f');
		setSegment(d, c, 'g');
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		break;

	case 5:
		setSegment(d, c, 'f');
		setSegment(d, c, 'a');
		setSegment(d, c, 'g');
		setSegment(d, c, 'c');
		setSegment(d, c, 'd');

		break;

	case 6:
		setSegment(d, c, 'g');
		setSegment(d, c, 'e');
		setSegment(d, c, 'c');
		setSegment(d, c, 'd');
		setSegment(d, c, 'f');
		break;

	case 7:
		setSegment(d, c, 'a');
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		break;

	case 8:
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		setSegment(d, c, 'd');
		setSegment(d, c, 'e');
		setSegment(d, c, 'f');
		setSegment(d, c, 'a');
		setSegment(d, c, 'g');
		break;

	case 9:
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		setSegment(d, c, 'f');
		setSegment(d, c, 'g');
		setSegment(d, c, 'a');
		break;

	case 0:
		setSegment(d, c, 'b');
		setSegment(d, c, 'c');
		setSegment(d, c, 'd');
		setSegment(d, c, 'e');
		setSegment(d, c, 'f');
		setSegment(d, c, 'a');
		break;

	default:
		break;
	}

	//turn on the dot?
	if (widthDot == true)
	{
		setSegment(d, c, 'h');
	}
}


// Set a segment to generate digit
void setSegment(struct clock_digit *d, struct rgb_color c, char s)
{
	/* Small quick fix for my own clock that has few darker segments in digit 1*/
 /*
	uint8_t adjst_val = 30;
	if (d->gpio == DIGIT_4_PIN) {
		if (s != 'b' && s != 'g') {
			if (c.r + adjst_val+15 <= 255) { c.r += adjst_val + 15; } else { c.r = 255; }
			if (c.g + adjst_val <= 255) { c.g += adjst_val; } else { c.g = 255; }
			//if (c.b + adjst_val <= 255) { c.b += adjst_val; } else { c.b = 255; }
		}
	}
*/
	switch (s)
	{
	case 'a':
		for (uint16_t i = d->seg_A_start; i < (d->seg_A_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;

	case 'b':
		for (uint16_t i = d->seg_B_start; i < (d->seg_B_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;

	case 'c':
		for (uint16_t i = d->seg_C_start; i < (d->seg_C_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;

	case 'd':
		for (uint16_t i = d->seg_D_start; i < (d->seg_D_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;

	case 'e':
		for (uint16_t i = d->seg_E_start; i < (d->seg_E_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;

	case 'f':
		for (uint16_t i = d->seg_F_start; i < (d->seg_F_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;

	case 'g':
		for (uint16_t i = d->seg_G_start; i < (d->seg_G_start + d->segment_pixels); i++) {
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;
	case 'h':
		for (uint16_t i = d->seg_DOT_start; i < (d->seg_DOT_start + 4); i++) {
			
			d->strip.setPixelColor(i, c.r, c.g, c.b);
		}
		break;
	default:
		break;
	}
}

//clear the clock leds
void clear_all()
{
	for (int i = 0; i < NUM_OF_DIGITS; i++) {
		clock_digits[i].strip.clear();
		clock_digits[i].strip.show();
	}
}

//change the clock dimming
void update_dimming()
{
	for (int i = 0; i < NUM_OF_DIGITS; i++) {
		clock_digits[i].strip.setBrightness(dim_level);
	}
}

//Check the environment light and change the clock dimming accordingly
void manage_dimming()
{
	uint16_t ldr_val = analogRead(A0);
	ldr_val = analogRead(A0);

	if (ldr_val < MIN_LDR_THRESH) {		//MIN_LDR = MAX LIGHT
		dim_level = BRIGHT_MAX_VALUE;
		update_dimming();
	}
	else if (ldr_val < (MAX_LDR_THRESH - 10) && ldr_val >(MIN_LDR_THRESH + 10)) { // +-10  hysteresis
		dim_level = BRIGHT_MED_VALUE;
		update_dimming();
	}
	else if (ldr_val > MAX_LDR_THRESH) { //MIN_LDR = MIN LIGHT
		dim_level = BRIGHT_MIN_VALUE;
		update_dimming();
	}
}

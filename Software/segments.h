//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 4.9.2017				//
// File Name: segments.h				//
//////////////////////////////////////////

#ifndef _SEGMENTS_h
#define _SEGMENTS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Adafruit_NeoPixel.h>
#include "types.h"
#include "settings.h"

#define NUM_OF_DIGITS 4

//Global Variables
extern const struct rgb_color color_red;	//red color value
extern const struct rgb_color color_green;  //green color value
extern const struct rgb_color color_blue;   //vlue color value
extern const struct rgb_color color_white;  //white color value
extern struct rgb_color clock_color; //current clock color
extern uint8_t dim_level; //diming level
extern clock_digit clock_digits[NUM_OF_DIGITS]; //Clock Digits


//Function Declecations
void setupPixels();
void setSegment(struct clock_digit *d, struct rgb_color c, char s);
void setNumber(struct clock_digit *d, struct rgb_color c, uint8_t n, bool widthDot = false);
void clear_all();
void manage_dimming();
void update_dimming();
#endif


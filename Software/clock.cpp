//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 19.9.2019				//
// File Name: clock.cpp					//
////////////////////////////////////////// 

#include "clock.h"
#include "clockTime.h"

//digits value global variables
static uint8_t d1 = 0, d2 = 0, d3 = 0, d4 = 0;
static uint8_t minutes = 0, seconds = 0; //used for stopwatch and countdown
uint8_t dim_level = 255;
extern uint8_t time_h, time_m;

//stopwatch and countdown states
states stopWatchState = start;
states countDownState = start;

ClockStates clockState = state_clock;

void flash_dot(uint8_t d)
{
	for (uint16_t i = clock_digits[d].seg_DOT_start; i < clock_digits[d].seg_DOT_start + 4; i++)
	{
		clock_digits[d].strip.setPixelColor(i, 0, 255, 0);
	}

	clock_digits[d].strip.show();
	myDelay(500);

	clock_digits[d].strip.clear();
	clock_digits[d].strip.show();
}

void digit_test(uint8_t d)
{
	clock_digits[d].strip.clear();
	for (uint16_t i = 0; i < clock_digits[d].strip.numPixels(); i++)
	{
		clock_digits[d].strip.setPixelColor(i, 0,255,0);
		clock_digits[d].strip.show();
		myDelay(100);
	}

	myDelay(1000);
}

void manage_clock()
{
	static uint64_t t = millis(); //time tracking
	static uint16_t count = 0;

	if ((millis() - t) > 1000U) {

		manage_dimming(); //check every second if the clocked should be dimmed

		//update the time from the RTC every 60 minutes
		if(count == 3600 ){
			getClockTime();
			count = 0;
		}
		count++;

    // time is updated using timer interrupt every second.
    // every ~1000ms the display is updated
    buildTimeDigits();
		
	  //false - do not turn on the dots, true - turn on dots
    if (count % 2){
      update_clock_digits(false);
    } else {
      update_clock_digits(true);
    }
		t = millis();
	}
}

void manage_countdown_timer()
{
		static uint64_t t = millis();
		static uint16_t count = 0;
		if (countDownState == pause) {
			return; //countdown is is paused
		}

		if(millis() - t > 1000) // 1 second
		{
			if (seconds == 0){
				if (minutes > 0){
					minutes--;
					seconds = 59;
				}
		}
		else{
			seconds--;
		}

		if (minutes == 0 && seconds == 0){
			count++;
			if (count % 2)
			{
				d1 = 0;
				d2 = 0;
				d3 = 0;
				d4 = 0;
				update_clock_digits(true); //widh dot
			}
			else{
				clear_all();
			}
		}
		else
		{
			split_digits(minutes, seconds);
			update_clock_digits(true);
		}

		t = millis();
	}
}

void manage_stopwatch()
{
	static uint64_t t = millis();

	if (stopWatchState == pause) {
		return; //stopwatch is is paused
	}

	if (millis() - t > 1000) // 1 second
	{
		if (seconds == 59) {
			if (minutes < 101){
				minutes++;
				if (minutes == 100){
					minutes = 0;
				}
				seconds = 0;
			}
		}
		else {
			seconds++;
		}

		split_digits(minutes, seconds);
		update_clock_digits(true); //widh dot
		t = millis();
	}

}

void set_starting_point(uint8 m, uint8_t s)
{
	minutes = m;
	seconds = s;
}

void split_digits(uint8_t m, uint8_t s)
{
	d1 = m / 10;
	d2 = m % 10;
	d3 = s / 10;
	d4 = s % 10;
}

void update_clock_digits(bool dot)
{
	clock_digits[0].strip.clear();
	setNumber(&(clock_digits[0]), clock_color, d1);
	clock_digits[0].strip.show();

	clock_digits[1].strip.clear();
	clock_digits[2].strip.clear();

	setNumber(&(clock_digits[1]), clock_color, d2, dot);
	setNumber(&(clock_digits[2]), clock_color, d3, dot);
	
	clock_digits[1].strip.show();
	clock_digits[2].strip.show();

	clock_digits[3].strip.clear();
	setNumber(&(clock_digits[3]), clock_color, d4);
	clock_digits[3].strip.show();
}

void buildTimeDigits()
{
  split_digits(time_h, time_m);
}

// Update EEPROM (address: byte 2 to 4 [r,g,b])
void updateEepromColor(rgb_color c) {
	EEPROM.begin(512);
	EEPROM.write(2, c.r);
	EEPROM.write(3, c.g); 
	EEPROM.write(4, c.b);

	EEPROM.end();
}
// Read EEPROM (address: byte 2 to 4 [r,g,b])
rgb_color readEepromColor() {
	rgb_color c;

	EEPROM.begin(512);
	c.r = EEPROM.read(2);
	c.g = EEPROM.read(3);
	c.b = EEPROM.read(4);

	EEPROM.end();

	return c;
}

bool colorCompare(rgb_color c1, rgb_color c2) {
	return (c1.b == c2.b) && (c1.g == c2.g) && (c1.r == c2.r);
}

void myDelay(uint64_t ms) {
	uint64_t currentMillis = millis();
	while (millis() - currentMillis < ms) {
		ESP.wdtFeed();
	}
}

//////////////////////////////////////////
// Autor: Itay Sperling					//
// Last Update: 24.3.2018				//
// File Name: web.cpp					//
//////////////////////////////////////////

#include "web.h"

extern uint8_t dstEnabled;
extern rgb_color clock_color;
extern uint8_t time_h, time_m, time_s;

#ifdef ENABLE_WEBSERVER
// Webserver
ESP8266WebServer server(80);
#endif //ENABLE_WEBSERVER

#ifdef ENABLE_OTA
	ESP8266HTTPUpdateServer httpUpdater;
#endif //ENABLE_OTA

#ifdef ENABLE_MQTT
	WiFiClient espClient;             // Wifi client (for connecting to home network)
	PubSubClient client(espClient);   // Mqtt client
#endif //ENABLE_MQTT

#ifdef ENABLE_WIFI
	bool wifiConnect()
	{
		static uint32_t last_try = 0;
		if (last_try == 0) {
      last_try = 1;
			// ESP WIFI CONFIG
			WiFi.hostname(CLOCK_NAME);
			WiFi.begin(WIFI_SSID, WIFI_PASS);
			Serial.print("Connecting to WiFi ");
		}
		if (WiFi.status() != WL_CONNECTED) {
			if((millis() - last_try > 500)){
				last_try = millis();
				Serial.print(".");
			}
			return false;
		}
		return true;
	}

	bool isWifiConnected()
	{
		if (WiFi.status() != WL_CONNECTED) {
			return false;
		} else {
			return true;
		}
	}

	bool setupWifi()
	{
		if (wifiConnect() == false) {
			//Start the clock without wifi functionality
			return false;
		}
		Serial.println("");
		Serial.print("Connected to ");
		Serial.println(WIFI_SSID);
		Serial.print("IP address: ");
		Serial.println(WiFi.localIP());

	#ifdef ENABLE_WEBSERVER
		server.on("/", handle_root);             //root page
		server.onNotFound(handleNotFound);       //page if not found
		server.begin();
		Serial.println("HTTP server started");

		#ifdef ENABLE_OTA
			setupHttpUpdaer(); //ota http updater
		#endif
	#endif
			return true;
	}
#endif

#ifdef ENABLE_OTA
//ota http updater
void setupHttpUpdaer()
{
  httpUpdater.setup(&server);
}
#endif //ENABLE_OTA

#ifdef ENABLE_WEBSERVER
void handleNotFound() {
	Serial.print("\t\t\t\t URI Not Found: ");
	server.send(200, "text/plain", "URI Not Found");
}

void handle_root() {
	//Build the webpage
	String toSend = html0;
  char rgbcolor[13];

	//Build the curerent time string
	char timeStr[10];
	snprintf(timeStr, sizeof(timeStr), "%02u:%02u:%02u", time_h, time_m, time_s);
	toSend.concat(timeStr);

	toSend.concat(html1);
  snprintf(rgbcolor, sizeof(rgbcolor), "%d, %d, %d", clock_color.r, clock_color.g, clock_color.b);
  toSend.concat(rgbcolor);
 
  toSend.concat(html2); 
	if (dstEnabled) {
		//If dst is enabled, check the checkbox
		toSend.concat(" checked");
	}
	toSend.concat(html3);

	if (server.hasArg("r") && server.hasArg("g") && server.hasArg("b")) {
		clock_color.r = server.arg("r").toInt();
		clock_color.g = server.arg("g").toInt();
		clock_color.b = server.arg("b").toInt();
	}
	else if (server.hasArg("dim")) {		
			dim_level = uint8_t((server.arg("dim").toInt())*2.55); //max val is 255
			update_dimming();
	}
	else if (server.hasArg("dst")) {
		dstEnabled = server.arg("dst").toInt();
		
		EEPROM.begin(512);
		EEPROM.write(0, dstEnabled); //Store the new DST value
		EEPROM.end();

		//Update the RTC module
//		Serial.println("Updating the time (DST)");

		//force new time request now with the new DST value
		ntpRequestTime();
	}

	server.send(200, "text/html", toSend);
}

void handleWebClient()
{
	server.handleClient();
}
#endif //ENABLE_WEBSERVER




#ifdef ENABLE_MQTT
void setupMQTT()
{
	// Mqtt Initialization
	client.setServer(MQTT_SERVER, MQTT_PORT);
	// Set Mqtt recieve callback function
	client.setCallback(mqttCallback);
}

//Mqtt message received
void mqttCallback(char* topic, byte* payload, unsigned int length) {
	char buff[32];
	uint8_t i;
	rgb_color temp_color = clock_color;


	Serial.print("Message arrived [");
	Serial.print(topic);
	Serial.print("] ");

	for (i = 0; i < length; i++) {
		Serial.print((char)payload[i]);
		buff[i] = (char)payload[i];
	}
	Serial.println();
	buff[i] = 0; //Termination char for working with strcmp



	//STOPWATCH
	if (strcmp(MQTT_TOPIC_STOPWATCH, topic) == 0){
		if (strstr(buff, "start") != NULL) {
			set_starting_point(0, 0);
			clockState = state_stopwatch;
			stopWatchState = start;
			Serial.println("Change to stopwatch");
		}
		else if (strstr(buff, "stop") != NULL) {
			clockState = state_clock;
			stopWatchState = stop;
		}
		else if ( strstr(buff, "pause") != NULL) {
			stopWatchState = pause;
		}
		else if (strstr(buff, "resume") != NULL) {
			stopWatchState = start;
		}
		else if ( strstr(buff, "reset") != NULL) {
			set_starting_point(0, 0);
			stopWatchState = start;
			clockState = state_stopwatch;
		}
	}

	//COUNTDOWN
	if (strcmp(MQTT_TOPIC_COUNTDOWN, topic) == 0) {
		if (strstr(buff, "stop") != NULL) {
			clockState = state_clock;
			countDownState = stop;
		}
		else if (strstr(buff, "pause") != NULL) {
			countDownState = pause;
		}
		else if (strstr(buff, "resume") != NULL) {
			countDownState = resume;
		}
	}

	//START COUNTDOWN (WITH NUMBER)
	if (strcmp(MQTT_TOPIC_COUNTDOWN_START, topic) == 0) {
		uint8_t ms = 0;
		uint8_t ss = 0;
		uint8_t count = 0;

		//convert char to uint8 (minutes)		
		while (buff[count] != ',' && buff[count] != 0)
		{
			ms *= 10;
			ms += buff[count] - '0';
			count++;
		}

		count++; //skip ','
		
		 //convert char to uint8 (seconds)
		while (buff[count] != 0)
		{
			ss *= 10;
			ss += buff[count] - '0';
			count++;
		}

		clockState = state_countdown;
		countDownState = start;
		set_starting_point(ms,ss);
	}

	//CHANGE COLOR BY NAME
	if (strcmp(MQTT_TOPIC_COLOR, topic) == 0) {
		if (strstr(buff, "red") != NULL) {
			clock_color = color_red;
		}
		else if (strstr(buff, "green") != NULL) {
			clock_color = color_green;
		}
		else if (strstr(buff, "blue") != NULL) {
			clock_color = color_blue;

		} else if (strstr(buff, "white") != NULL) {
			clock_color = color_white;
		}
		if (!colorCompare(clock_color,temp_color)) { //only if color was change - avoid many eeprom writes
			updateEepromColor(clock_color);
		}
	}


	//CHANGE COLOR BY VALUE
	if (strcmp(MQTT_TOPIC_RGB, topic) == 0) {
		rgb_color c = { 0,0,0 };
		uint8_t count = 0;

		//convert RED to uint8	
		while (buff[count] != ',' && buff[count] != 0)
		{
			c.r *= 10;
			c.r += buff[count] - '0';
			count++;
		}
		count++; //skip ','

		 //convert GREEN to uint8	
		while (buff[count] != ',' && buff[count] != 0)
		{
			c.g *= 10;
			c.g += buff[count] - '0';
			count++;
		}
		count++; //skip ','

		//convert GREEN to uint8	
		while (buff[count] != 0)
		{
			c.b *= 10;
			c.b += buff[count] - '0';
			count++;
		}

		clock_color = c;
		if (!colorCompare(clock_color, temp_color)) {
			updateEepromColor(c);
		}
	}	
}


void handleMQTT()
{
	//Check if Mqtt client disconnected
	if (!client.connected()) {
		mqttReconnect();
	}

	//handle Mqtt
	client.loop();
}

void mqttReconnect() {
	static uint64_t last_try = millis() - 5001U;

	// Wait 5 seconds before retrying
	if (!client.connected()) {
		if((millis() - last_try) > 5000U){
			Serial.print("Attempting MQTT connection...");
			// Attempt to connect
			if (client.connect(MQTT_CLIENTID, MQTT_CLIENTUSERNAME, MQTT_CLIENTPASS)) {
				Serial.println("connected");
				// Once connected, publish an announcement...
				// ... and resubscribe
				client.subscribe(MQTT_TOPIC);
			}
			else {
				Serial.print("failed, rc=");
				Serial.print(client.state());
				Serial.println(" try again in 5 seconds");			
			}

			last_try = millis();
		}
	}
}
#endif //ENABLE_MQTT

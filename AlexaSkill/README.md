# Alexa Seven Segment Clock Skill
Controls the clock features: 
* Countdown timer (max time is 99 minutes and 99 seconds)
* Stopwatch
* Color Change

```
Alexa, ask the clock to change the color to {Green/Red/Blue/White}.
Alexa, ask the clock to {Start/Stop/Pause/Resume} a stopwatch.
Alexa, ask the clock to Start a {5 mintues/70 seconds/1 hour} countdown.
Alexa, ask the clock to {Stop/Pause/Resume} the countdowm.
```


The skill connects to the clock via MQTT client.

## dependencies ##
* alexa-sdk
* mqtt
 
Make sure you install them and upload them with the skill.  
They should be located in the 'node_modules' folder.  
Just make a zip file with the skill and the 'node_modules' folder,  
and upload to you AWS Lambda function. 
  
When you have node.js installed on your machine, you can install  
the dependencies with the following commands: 
```
npm install mqtt  
npm install alexa-sdk
```
'use strict';

var Alexa = require('alexa-sdk');
var mqtt = require('mqtt');
var APP_ID = '';

//mqtt settings
var options = {
    host: 'YOUR_MQTT_SERVER',
    port: 11100,
    clientId: 'alexa_skill_clock' +  Math.random().toString(16).substr(2, 8),
    username: 'YOUR_MQTT_USERNAME',
    password: 'YOUR_MQTT_PASSWORD'
    };

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    alexa.resources = languageStrings;
    alexa.registerHandlers(handlers);
    alexa.execute();
};


var handlers = {
    'LaunchRequest': function () {
        this.attributes['speechOutput'] = this.t("WELCOME_MESSAGE", this.t("SKILL_NAME"));
        // If the user either does not reply to the welcome message or says something that is not
        // understood, they will be prompted again with this text.
        this.attributes['repromptSpeech'] = this.t("WELCOME_REPROMPT");
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    },
    'StopwatchIntent': function () {
        var itemSlot = this.event.request.intent.slots.stopWatchCommands; //'stopWatchCommands' is the slot name
        var itemName;
  
        if (itemSlot && itemSlot.value) {
            itemName = itemSlot.value.toLowerCase();              

            mqttPublish('7SegClk','stopwatch',itemName,'',this);
        }
        else
        {
            this.attributes['speechOutput'] = this.t("REPEAT");
            this.emit(':ask', this.attributes['speechOutput']); 
        }
    }, //end stopwatch
 'CountdownIntent': function () {
        var itemSlotCommand = this.event.request.intent.slots.countDownCommands; //'stopWatchCommands' is the slot name
        var itemSlotDuration = this.event.request.intent.slots.time; //'time' is the slot name

        var itemNameDuration;
        if(itemSlotDuration.value.includes("PT")){
                var timeStr = (itemSlotDuration.value.split("PT"))[1];
                var timeH = '0';
                var timeM = '0';
                var timeS = '0';
                // String is ISO-8601 duration format (PnYnMnDTnHnMnS)
                //I only accept Time, so it must start with PT
                //The loop is storing the chars until it reads H, M or S chars.
                //When it reads, it copies the stored value to the corret varaible
                var tempStr='';
                for(var i=0; i<timeStr.length; i++)
                    {        
                        if(timeStr[i] == 'H'){
                            timeH = tempStr;
                            tempStr = '';
                        } else if(timeStr[i] == 'M'){
                            timeM = tempStr;
                            tempStr = '';
                        } else if(timeStr[i] == 'S'){
                            timeS = tempStr;                         
                        } else{
                            tempStr += timeStr[i];
                        }
                    }
                //Max clock timer is 99 minutes and 99 seconds,
                //hours are coverted to minutes.
                var minutes = (parseInt(timeH)*60 + parseInt(timeS));
                var seconds = parseInt(timeS);
                if(seconds > 59)
                    {
                        minutes += parseInt(seconds / 60);
                        seconds = (seconds % 60);
                    }
                itemNameDuration = minutes.toString() + "," + seconds.toString();                
            }

        var itemNameCommand;
        if (itemSlotCommand && itemSlotCommand.value) {
            itemNameCommand = itemSlotCommand.value.toLowerCase();

            mqttPublish('7SegClk','countdown',itemNameCommand, ',' + itemNameDuration,this);

        }
        else
        {
            this.attributes['speechOutput'] = this.t("REPEAT");
            this.emit(':ask', this.attributes['speechOutput']); 
        }
    }, //end countdown
        'ChangeColorIntent': function () {
        var itemSlot = this.event.request.intent.slots.color; //'color' is the slot name
        var itemName;
  
        if (itemSlot && itemSlot.value) {
            itemName = itemSlot.value.toLowerCase();
        
              mqttPublish('7SegClk','color',itemName,'',this);
        }
        else
        {
            this.attributes['speechOutput'] = this.t("REPEAT");
            this.emit(':ask', this.attributes['speechOutput']); 
        }
    }, //end color
    'AMAZON.HelpIntent': function () {
        this.attributes['speechOutput'] = this.t("HELP_MESSAGE");
        this.attributes['repromptSpeech'] = this.t("HELP_REPROMPT");
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    },
    'AMAZON.RepeatIntent': function () {
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    },
    'AMAZON.StopIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'AMAZON.CancelIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'SessionEndedRequest':function () {
        this.emit(':tell', this.t("STOP_MESSAGE"));
    },
    'Unhandled': function () {
        this.attributes['speechOutput'] = this.t("HELP_MESSAGE");
        this.attributes['repromptSpeech'] = this.t("HELP_REPROMPT");
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    }
};

//topic - mqtt tpoid
//subject - clock feater (color, stopwatch etc..)
//msgPrimary - the main paramater (like start, stop, pause, blue, red)
//msgSecondary - the secondery paramater (like 90 second, 1 hour..)
//caller (allias to the handler)
function mqttPublish(topic,subject,msgPrimary, msgSecondary, caller){
    
		    // Pushlish to mqtt broker
            var mqttPromise = new Promise(function(resolve, reject) {
            var client = mqtt.connect(options);

            client.on('connect', function() { 
                // When connected to mqtt broker,
                // publish a message with the topic '7SegClk'
                    client.publish(topic, subject + ',' + msgPrimary + msgSecondary, function() {
                        client.end();
                        resolve('published succesfully');
                    });
                });
            });

            mqttPromise.then(
                //resolve
                function(data) {
                    console.log(data);		                    

                    if(subject == 'color'){
                      caller.attributes['speechOutput'] = 'Color changed to ' + msgPrimary;
                        } else {
                        if(msgPrimary == 'stop') { msgPrimary+='p';}
                                                
                            if(msgPrimary.slice(-1) == 'e') {
                                caller.attributes['speechOutput'] = subject + ' ' + msgPrimary + "d";
                            }
                            else{
                                caller.attributes['speechOutput'] = subject +  ' ' + msgPrimary + "ed";
                            }
                    }

                    //spech,card tittle, card content, image
                    caller.emit(':tellWithCard', caller.attributes['speechOutput'], caller.attributes['speechOutput'], caller.attributes['speechOutput']);
                },
                //reject
                function(err) {
                    console.log('An error occurred:', err);
                     caller.attributes['speechOutput'] = caller.t("ERROR");
                    caller.emit(':tell', self.attributes['speechOutput']);
                });
}

var languageStrings = {
    "en": {
        "translation": {
            "SKILL_NAME": "Clock",
            "WELCOME_MESSAGE": "Welcome to the clock skill, You can ask the clock to start a countdown a stopwatch, or change the color",
            "WELCOME_REPROMPT": "Ask the clock to start a stopdwatch.",
            "DISPLAY_CARD_TITLE": "%s  - Starting %s ed.",
            "HELP_MESSAGE": "You can ask the clock to start a stopdwatch or a countdown timer",
            "REPEAT" :"Please repeat your request.",
            "HELP_REPROMPT": "You can say things like, ask to door lock itself... Now, what can I help you with?",
            "ERROR" :"There was a problem with your request. Please try again later.",
            "STOP_MESSAGE": "Goodbye!",
        }
    }
};


